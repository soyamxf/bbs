<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>BBS</title>
<link rel="stylesheet" href="materialize/css/materialize.min.css">
<link href="iconfont/material-icons.css" rel="stylesheet">
<link href="css/common.css" rel="stylesheet">
<link href="css/note.css" rel="stylesheet">
<script src="js/jquery-2.2.2.min.js"></script>
<script src="materialize/js/materialize.min.js"></script>
<script src="js/note.js"></script>
<script src="js/ajax.js"></script>
</head>

<body>
	<jsp:include page="nav.jsp"></jsp:include>
	<div class="container">
		<div id="notes">
			<%
				String mainId = request.getParameter("mainId");
			%>
		</div>
		<div class="new-note card-panel z-depth-3">
			<form method="post" action="reply">
				<input type="hidden" name="mainId" value="<%=mainId%>"> 
				<div class="input-field">
					<i class="material-icons prefix">mode_edit</i>
					<textarea name="content" id="note-content" class="materialize-textarea" required></textarea>
					<label for="note-content">你想说的话？</label>
				</div>

				<div class="right-align">
					<button class="btn waves-effect waves-light green lighten-1"
						type="submit" name="action">
						发表 <i class="material-icons">send</i>
					</button>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		loadNextPage(
			"content?mainId=" + <%=mainId%>+"&page=1"
			, "#notes");
		$("form").submit(function() {
			var $action = $(this).attr("action");
			$.post($action, $(this).serialize(), function(data, status) {
				if (data.match("success") != null) {
					loadNextPage( "content?mainId=" + <%=mainId%>+"&page=1"
							, "#notes")
					Materialize.toast("发送成功", 2000);
					document.getElementsByTagName("form")[0].reset();
				}else if(data.match("error") != null){
					location.href="login";
				}
			})
			return false;
		});
	</script>
	<jsp:include page="footer.jsp"></jsp:include>
</body>