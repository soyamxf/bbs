<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>BBS</title>
<link rel="stylesheet" href="materialize/css/materialize.min.css">
<link href="iconfont/material-icons.css" rel="stylesheet">
<link href="css/common.css" rel="stylesheet">
<script src="js/jquery-2.2.2.min.js"></script>
<script src="materialize/js/materialize.min.js"></script>
<script src="js/ajax.js"></script>
<script src="js/rank.js"></script>
</head>

<body>
	<jsp:include page="nav.jsp"></jsp:include>
	<div class="container min-height-800">
		<table class="striped centered">
			<thead>
				<tr>
					<th data-filed="rank" class="width-15">排名</th>
					<th data-filed="name" class="width-35">昵称</th>
					<th data-filed="level" class="width-25">等级</th>
					<th data-filed="exp" class="width-25">经验</th>
				</tr>
			</thead>
			<tbody id="u">
				<tr>
					<td colspan="4"></td>
				</tr>
				<tr>
					<td colspan="4"><jsp:include page="wait.html"></jsp:include></td>
				</tr>
			</tbody>
		</table>

	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>