<%@page import="java.util.ArrayList"%>
<%@page import="top.soyask.bbs.domain.Page"%>
<%@page import="top.soyask.bbs.domain.User"%>
<%@page import="top.soyask.bbs.domain.Note"%>
<%@page import="top.soyask.bbs.domain.container.KeyValue"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	List<KeyValue<Note, User>> notes = (List<KeyValue<Note, User>>) request.getAttribute("subNote");
	List<Long> fansId = (List<Long> )session.getAttribute(User.FOLLOW);	
	if(fansId == null){
		fansId = new ArrayList();
	}
	if(notes != null){
		for (KeyValue<Note, User> kv : notes) {
			Note note = kv.getKey();
			User user = kv.getValue();
			if (note.getTitle() != null) {
%>
<div class="row">
	<div class="col l3 m4 s12 user-info hide-on-small-only"
		data-target="note-1">
		<div class="card-panel">
			<div class="right-align">
				<a href="user?uid=<%=user.getUid() %>"> <img src="<%=user.getPicPath() %>"
					class="responsive-img circle head-small" />
				</a>
			</div>
			<a href="follow?uid=<%=user.getUid() %>" class="right follow"> 
				<i class="material-icons middle green-text">
					<%=fansId.contains(user.getUid())?"favorite":"favorite_border" %>	
				</i>
			</a>
			<div class="left-align">
				<a href="user?uid=<%=user.getUid() %>" class="brown-text darken-1"> <b><%=user.getUname() %></b>
				</a>
				<p class="grey-text smaller-font"><%=user.getInfo() %></p>
				<ul class="hide-on-small-only">
					<li class="green-text text-light">Lv<%=user.getExp()/100%></li>
					<li class="smaller-font "><a href="#"
						class="red-text text-light-1">经验：<%=user.getExp() %></a></li>
					<li class="smaller-font"><a href="#"
						class="orange-text text-light-1">主题：123456</a></li>
					<li class="smaller-font"><a href="#" class="deep-purple-text">粉丝：<%=user.getFans() %></a></li>
					<li></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col l9 m8 s12 card" id="note-1">
		<div class="card-content">
			<span class="card-title note-title"><%=note.getTitle() %></span>
				<div class="divider hide-on-small-only"></div>
			 <a href="user?uid=<%=user.getUid() %>" class="little-margin hide-on-med-only hide-on-med-and-up"> 
			 <img src="<%=note.getuPicPath() %>" class="responsive-img circle head-tiny"
				align="bottom" /> 
				<span class="little-margin"> <%=note.getUname() %></span>
				<div class="divider"></div>
			</a>
			<p class="note-info">
				<%=note.getContent() %>
			</p>
			<div class="center-align hide-on-small-only ">
				<a id="up" href="ud?type=1&mainId=<%=note.getMainId() %>" class=" btn-large btn-floating waves-effect grey"> 
				<i class="material-icons">thumb_up</i>
				</a> <a id="down" href="ud?type=0&mainId=<%=note.getMainId() %>"
					class=" btn-large btn-floating waves-effect waves-light grey">
					<i class="material-icons">thumb_down</i>
				</a>
				<script src="js/up.js"></script>
			</div>
			<div class="right-align">
				<a href="#" class="smaller-font little-margin">分享</a> <a href="#"
					class="smaller-font little-margin">举报</a> <span
					class="smaller-font little-margin"><%=note.getFloor() %>楼</span> 
					<span class="little-margin smaller-font"><%=note.getCreateDate() %></span>
			</div>
		</div>
	</div>
</div>
<%
	} else {
%>
<div class="row">
	<div class="col l3 m4 s12 user-info hide-on-small-only"
		data-target="note-1">
		<div class="card-panel">
			<div class="right-align">
				<a href="user?uid=<%=user.getUid() %>"> <img src="<%=user.getPicPath() %>"
					class="responsive-img circle head-small" />
				</a>
			</div>
			<a href="follow?uid=<%=user.getUid() %>" class="right follow"> 
				<i class="material-icons middle green-text">
					<%=fansId.contains(user.getUid())?"favorite":"favorite_border" %>	
				</i>
			</a>
			<div class="left-align">
				<a href="user?uid=<%=user.getUid() %>" class="brown-text darken-1"> <b><%=user.getUname() %></b>
				</a>
				<p class="grey-text smaller-font"><%=user.getInfo() %></p>
				<ul class="hide-on-small-only">
					<li class="green-text text-light">Lv<%=user.getExp()/100%></li>
					<li class="smaller-font "><a href="#"
						class="red-text text-light-1">经验：<%=user.getExp() %></a></li>
					<li class="smaller-font"><a href="#"
						class="orange-text text-light-1">主题：123456</a></li>
					<li class="smaller-font"><a href="#" class="deep-purple-text">粉丝：<%=user.getFans() %></a></li>
					<li></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col l9 m8 s12 card" id="note-1">
		<div class="card-content">
			 <a href="user?uid=<%=user.getUid() %>" class="little-margin hide-on-med-only hide-on-med-and-up"> 
			 <img src="<%=note.getuPicPath() %>" class="responsive-img circle head-tiny"
				align="bottom" /> 
				<span class="little-margin"> <%=note.getUname() %></span>
				<div class="divider"></div>
			</a>
			<p class="note-info">
				<%=note.getContent() %>
			</p>
	
			<div class="right-align">
				<a href="#" class="smaller-font little-margin">回复</a> <a href="#"
					class="smaller-font little-margin">举报</a> <span
					class="smaller-font little-margin"><%=note.getFloor() %>楼</span> <span
					class="little-margin smaller-font"><%=note.getCreateDate() %></span>
			</div>
		</div>
	</div>
</div>
<%
	}
	}
	}
	Page pages = (Page) request.getAttribute("page");
	if (pages != null) {
		String mainId = request.getParameter("mainId");
%>
<ul class="pagination">
	<li class="<%=pages.isFirst() ? "disabled" : "waves-effect"%>">
	<a class="load" data-for="notes"  href="content?mainId=<%=mainId %>&page=1"><i class="material-icons">chevron_left</i></a></li>
	<%
		List<Integer> p = pages.getRows();
			int current = pages.getCurrentPage();
			for (int str : p) {
	%>

	<li class="<%=str == current ? "active green lighten-1" : "waves-effect"%>">
		<a class="load" data-for="notes" href="content?mainId=<%=mainId %>&page=<%=str%>"><%=str%></a>
	</li>
	<%
		}
	%>
	<li class="<%=pages.isLast() ? "disabled" : "waves-effect"%>">
		<a class="load" data-for="notes"  href="content?mainId=<%=mainId %>&page=<%=pages.getCount()%>" >
			<i class="material-icons">chevron_right</i>
		</a>
	</li>
</ul>
<%
	}
%>
<script src="js/page.js"></script>
<script src="js/follow.js"></script>
