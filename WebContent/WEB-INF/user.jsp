<%@page import="top.soyask.bbs.domain.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>BBS</title>
<link rel="stylesheet" href="materialize/css/materialize.min.css">
<link href="iconfont/material-icons.css" rel="stylesheet">
<link href="css/common.css" rel="stylesheet">
<link href="css/user.css" rel="stylesheet">
<script src="js/jquery-2.2.2.min.js"></script>
<script src="materialize/js/materialize.min.js"></script>
<script src="js/ajax.js"></script>
<script src="js/user.js"></script>
</head>

<body>
	<jsp:include page="nav.jsp"></jsp:include>
	<div class="container">
		<div class="card-panel">
			<%
				User user = (User) session.getAttribute(User.CURRENT_USER);
				if (user != null) {
			%>
			<div class="center-align">
				<a href="#"> <img src="<%=user.getPicPath()%>"
					class="responsive-img circle head-large" />
				</a>
			</div>

			<div class="center-align">
				<a href="#" class="brown-text darken-1"><h3><%=user.getUname()%></h3></a>
				<p class="grey-text">
					<a href="#" id="edit" data-target="edit-dialog"
						class="waves-effect waves-green  little-margin"> <i
						class="material-icons tiny green-text">edit</i>

					</a>
					<%=user.getInfo() != null ? user.getInfo() : "这家伙很懒，没有留下什么..."%>
				</p>
				<div class="progress">
					<div class="determinate green lighten-2"
						style="width: <%=user.getExp() / 1000%>%"></div>
				</div>
				<ul>
					<li><span class="level green white-text little-margin">Lv<%=user.getExp() / 100%></span><a
						href="#" class="green-text text-light-1">经验：<%=user.getExp()%></a></li>
				</ul>
			</div>
			<%
				} else {
			%>
			<div class="center-align">
				<a href="login"> <img src="imgs/default.png"
					class="responsive-img circle head-large" />
				</a>
			</div>

			<div class="center-align">
				<a href="login" class="brown-text darken-1"><h3>未登录</h3></a>
				<p class="grey-text">
					<a href="#" id="edit" data-target="edit-dialog"
						class="waves-effect waves-green  little-margin"> <i
						class="material-icons tiny green-text">edit</i>

					</a> 请登录...
				</p>
				<div class="progress">
					<div class="determinate green lighten-2" style="width: 0%"></div>
				</div>
				<ul>
					<li><span class="level green white-text little-margin">Lv0</span><a
						href="#" class="green-text text-light-1">经验：-1</a></li>
				</ul>
			</div>
			<%
				}
			%>


		</div>
	</div>
	<div class="container">
		<ul class="tabs tabs-transparent green lighten-1">
			<li class="tab"><a class="active" href="#news">最近</a></li>
			<li class="tab"><a href="#follow">我的关注</a></li>
			<li class="tab"><a href="#fans">我的粉丝</a></li>
		</ul>
		<div class="min-height-500" id="news">
			<jsp:include page="wait.html"></jsp:include>
		</div>
		<div class="min-height-500" id="follow">
			<jsp:include page="wait.html"></jsp:include>
		</div>
		<div class="min-height-500" id="fans">
			<jsp:include page="wait.html"></jsp:include>
		</div>
		<div id="edit-dialog" class="modal green-text">
			<div class="modal-content">
				<h5>修改信息</h5>
				<form action="#" method="post" class="section">
					<input type="hidden" name="head">
					<div class="file-field input-field">
						<img src=""
							class="responsive-img  border-1 circle head-small  min-height-100  max-height-100"
							id="input-head"> <input type="file" name="head"
							accept="image/jpg,image/jpeg,image/gif,image/x-png">
					</div>
					<div class="input-field">
						<input type="text" id="un" required> <label for="un">昵称</label>
					</div>
					<div class="input-field">
						<input type="text" id="pd" required> <label for="pd">个性签名</label>
					</div>
					<br>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#!"
					class=" modal-action modal-close waves-effect waves-green btn-flat green-text">
					取消 </a> <a href="#!"
					class=" modal-action modal-close waves-effect waves-green btn-flat green-text">
					修改 </a>
			</div>
		</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>