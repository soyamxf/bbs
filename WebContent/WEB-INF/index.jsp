<%@page import="top.soyask.bbs.domain.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>BBS</title>
<link rel="stylesheet" href="materialize/css/materialize.min.css">
<link href="iconfont/material-icons.css" rel="stylesheet">
<link href="css/common.css" rel="stylesheet">
<link href="css/index.css" rel="stylesheet">
<script src="js/jquery-2.2.2.min.js"></script>
<script src="materialize/js/materialize.min.js"></script>
<script src="js/ajax.js"></script>
<script src="js/index.js"></script>
<script src="js/post.js"></script>
</head>
<body>
	<div id="top" class="">
		<nav class="green lighten-2 nav-extended">
			<div class="nav-wrapper">
				<div class="container">
					<a href="#" class="brand-logo">BBS</a>
					<ul id="nav-mobile" class="right hide-on-med-and-down">
						<li><a href="index">帖子</a></li>
						<li><a href="rank">排行榜</a></li>
						<li><a href="user">我</a></li>
					</ul>
				</div>
				<a href="#" data-activates="slide-out" class="button-collapse">
					<i class="material-icons">menu</i>
				</a>
			</div>
			<ul id="slide-out" class="side-nav">
				<li>
					<%
						User user = (User) session.getAttribute(User.CURRENT_USER);
						if (user != null) {
					%>
					<div class="userView">
						<div class="background">
							<img src="<%=user.getPicPath()%>" />
						</div>

						<a href="#!user"><img class="circle"
							src="<%=user.getPicPath()%>" /></a> <a href="#!name"><span
							class="waves-effect white-text name"><%=user.getUname()%></span></a>
						<a href="#!email"><span class="white-text smaller-font">这个人很懒，并没有留下什么..</span></a>
					</div> <%
						 	} else {
						 %>
					<div class="userView">

						<div class="background">
							<!-- <img src="imgs/default.png" /> -->
						</div>

						<a href="login"><img class="circle"
							src="imgs/default.png" /></a> <a href="login"><span
							class="waves-effect white-text name">未登录</span></a>
						<a href="#!email"><span class="white-text smaller-font">请登录...</span></a>
					</div> <%
					 	}
					 %>
				</li>
				<li><a href="index" class="waves-effect">帖子</a></li>
				<li><a href="rank" class="waves-effect">排行榜</a></li>
				<li><a href="user" class="waves-effect">我</a></li>
			</ul>
			<ul class="tabs tabs-transparent">
				<div class="container">
					<li class="tab"><a class="active" href="#new-items">最新</a></li>
					<li class="tab"><a href="#hot-items">最热</a></li>
					<li class="tab"><a href="#best-items">精品</a></li>
				</div>
			</ul>


			<a href="#" data-actives="slide-out" class="button-collapse"> <i
				class="material-icons">menu</i>
			</a>
		</nav>

	</div>
	<div class="container">
		<div class="row">
			<!--左半边内容 帖子列表-->
			<div class="col l9 m12 s12">
				<div id="new-items">
					<jsp:include page="wait.html"></jsp:include>
				</div>
				<div id="hot-items">
					<jsp:include page="wait.html"></jsp:include>
				</div>
				<div id="best-items">
					<jsp:include page="wait.html"></jsp:include>
				</div>

				<a id="bottom"></a>
				<div class="new-note card-panel z-depth-3">
					<form method="post" action="report">
						<div class="input-field">
							<i class="tiny material-icons prefix ">translate</i> <input
								id="note-title" type="text" name="title" class="blue-text"
								required /> <label for="note-title">你帖子的标题？</label>
						</div>
						<div class="input-field">
							<i class="material-icons prefix">mode_edit</i>
							<textarea name="content" id="note-content" class="materialize-textarea" required></textarea>
							<label for="note-content">你想说的话？</label>
						</div>
						<div class="right-align">
							<button class="btn waves-effect waves-light green lighten-1"
								type="submit" name="action">
								发表 <i class="material-icons">send</i>
							</button>
						</div>
					</form>
				</div>
			</div>
			<!--右半边内容-->
			<div class="col m3 hide-on-med-and-down right-block">
				<div class="card-panel">
					<%
						if (user != null) {
					%>
					<div class="center-align">
						<a href="#"> <img src="<%=user.getPicPath()%>"
							class="responsive-img circle head-mid" />
						</a>
					</div>
					<div class="center-align">
						<a href="#" class="brown-text darken-1 little-margin"><b><%=user.getUname()%></b></a>
						<span class="level green white-text ">Lv<%=user.getExp() / 100%></span>
						<p class="grey-text smaller-font"><%=user.getInfo()%></p>
					</div>
					<%
						} else {
					%>
					<div class="center-align">
						<a href="login"> <img src="imgs/default.png"
							class="responsive-img circle head-mid" />
						</a>
					</div>
					<div class="center-align">
						<a href="login" class="brown-text darken-1 little-margin"><b>未登录</b></a>
						<p class="grey-text smaller-font">请先登录</p>
					</div>
					<%
						}
					%>

				</div>

				<div class="card-panel">
					<div class="chip">mxf</div>
					<div class="chip">mxf</div>
					<div class="chip">mxf</div>
					<div class="chip">mxf</div>
					<div class="chip">mxf</div>
					<div class="chip">mxf</div>
					<div class="chip">mxf</div>
					<div class="chip">mxf</div>
				</div>
			</div>
		</div>

		<div class="fixed-action-btn horizontal">
			<a class="btn-floating btn-large green light-2"> <i
				class="material-icons">menu</i>
			</a>
			<ul>
				<a href="#bottom" class="edit-note">
					<li class="btn-floating red"><i class="material-icons">mode_edit</i></li>
				</a>
				<a href="#top">
					<li class="btn-floating black"><i class="material-icons">vertical_align_top</i>
				</li>
				</a>
				<li class="btn-floating blue"><i class="material-icons">share</i></li>
			</ul>
		</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>