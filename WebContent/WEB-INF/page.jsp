<%@page import="java.util.List"%>
<%@page import="top.soyask.bbs.domain.Page"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	Page pages = (Page) request.getAttribute("page");
	if (pages != null) {
%>
<ul class="pagination">
	<li class="<%=pages.isFirst() ? "disabled" : "waves-effect"%>"><a
		href="#!"><i class="material-icons">chevron_left</i></a></li>
	<%
		List<Integer> p = pages.getRows();
			int current = pages.getCurrentPage();
			for (int str : p) {
	%>

	<li
		class="<%=str == current ? "active green lighten-1" : "waves-effect"%>">
		<a href="#!"><%=str%></a>
	</li>
	<%
		}
	%>
	<li class="<%=pages.isLast() ? "disabled" : "waves-effect"%>"><a
		href="#!"><i class="material-icons">chevron_right</i></a></li>
</ul>
<%
	}
%>