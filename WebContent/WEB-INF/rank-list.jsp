<%@page import="top.soyask.bbs.domain.Page"%>
<%@page import="top.soyask.bbs.domain.User"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	List<User> users = (List<User>) request.getAttribute("users");
	int i = 0;
	if (users != null) {

		for (User user : users) {
%>
<tr class="">
	<td data-filed="rank">
		<div class="green-text lighten-2"><%=++i%></div>
	</td>
	<td data-filed="name"><a href="#"><%=user.getUname()%></a></td>
	<td data-filed="level"><span class="level green white-text">Lv<%=user.getExp() / 100%></span></td>
	<td data-filed="exp"><%=user.getExp()%></td>
</tr>
<%
	}
	}
%>

<tr class="white">
	<td colspan="4">
		<div class="divider"></div> <%
 	Page pages = (Page) request.getAttribute("page");
 	if (pages != null) {
 		int current = pages.getCurrentPage();
 %>
		<ul class="pagination">
			<li class="<%=pages.isFirst() ? "disabled" : "waves-effect"%>">
			<a class ="load" data-for="u" href="next-rank?page=<%=current - 1%>"><i class="material-icons">chevron_left</i></a></li>
			<%
				List<Integer> p = pages.getRows();
					for (int str : p) {
			%>

			<li class="<%=str == current ? "active green lighten-1" : "waves-effect"%>">
				<a class ="load" data-for="u" href="next-rank?page=<%=str%>" onclick="return click()">
					<%=str%>
				</a>
			</li>
			<%
				}
			%>
			<li class="<%=pages.isLast() ? "disabled" : "waves-effect"%>">
				<a class ="load" data-for="u" href="next-rank?page=<%=pages.getCount()%>">
					<i class="material-icons">chevron_right</i>
				</a>
			</li>
		</ul> <%
 	}
 %>
	</td>
</tr>
<script src="js/page.js"></script>
