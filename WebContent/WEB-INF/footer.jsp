<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<footer class="page-footer green lighten-2">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">期末求过</h5>
                    <p class="grey-text text-lighten-4">还请老师高台贵手，放我一马</p>
                </div>
                <div class="col l4 offset-l2 s12">
                    <h5 class="white-text">友情链接</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="http://star.soyask.top/">无耻的写上自己的网站</a></li>
                        <li><a class="grey-text text-lighten-3" href="https://www.baidu.com/">baidu</a></li>
                        <li><a class="grey-text text-lighten-3" href="http://star.soyask.top/">再次无耻的写上自己的网站</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2017 Copyright soya·mxf  soyask.top
                <a class="grey-text text-lighten-4 right" href="#!">没有更多链接</a>
            </div>
        </div>
    </footer>