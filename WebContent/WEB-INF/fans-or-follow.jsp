<%@page import="java.util.ArrayList"%>
<%@page import="top.soyask.bbs.domain.Page"%>
<%@page import="top.soyask.bbs.domain.User"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<ul class="collection">
	<%
		List<User> users = (List<User>) request.getAttribute("users");
		List<Long> fansId = (List<Long> )session.getAttribute(User.FOLLOW);	
		if(fansId == null){
			fansId = new ArrayList();
		}
		for (User user : users) {
	%>
	<li class="collection-item avatar"><img
		src="<%=user.getPicPath()%>" alt="" class="circle"> <a
		href="user?uid=<%=user.getUid()%>" class="title"><%=user.getUname()%></a>

		<p class="smaller-font grey-text ">
			<%=user.getInfo() != null ? user.getInfo() : "这家伙很懒，没有留下什么..."%></p> <span
		class="smaller-font  amber-text text-darken-4"> 关注： <a href=""
			class="green-text little-margin"><%=user.getFollow()%></a> 粉丝： <a
			href="" class="green-text"><%=user.getFans()%></a>
	</span> <a href="follow?uid=<%=user.getUid() %>" class="right follow"> 
				<i class="material-icons middle green-text">
					<%=fansId.contains(user.getUid())?"favorite":"favorite_border" %>	
				</i>
			</a>
	<%
		}
		Boolean fans = (Boolean) request.getAttribute("fans");
		Page pages = (Page) request.getAttribute("page");
		if (pages != null) {
	%>
	<ul class="pagination">
		<li class="<%=pages.isFirst() ? "disabled" : "waves-effect"%>"><a
			class="load" data-for="<%=fans != null ? "fans" : "follow"%>"
			href="<%=fans != null ? "user-fans" : "user-follow"%>?page=1"><i class="material-icons">chevron_left</i></a></li>
		<%
			List<Integer> p = pages.getRows();
				int current = pages.getCurrentPage();
				for (int str : p) {
		%>

		<li
			class="<%=str == current ? "active green lighten-1" : "waves-effect"%>">
			<a class="load" data-for="<%=fans != null ? "fans" : "follow"%>"
			href="<%=(fans != null ? "user-fans" : "user-follow") + "?page="+str%>"><%=str%></a>
		</li>
		<%
			}
		%>
		<li class="<%=pages.isLast() ? "disabled" : "waves-effect"%>"><a
			class="load" data-for="<%=fans != null ? "fans" : "follow"%>" href="<%=fans != null ? "user-fans" : "user-follow"%>?page=0"><i
				class="material-icons">chevron_right</i></a></li>
	</ul>
	<%
		}
	%>

</ul>

<script src="js/page.js"></script>
<script src="js/follow.js"></script>
