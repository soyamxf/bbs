<%@page import="top.soyask.bbs.domain.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="top" class="">
	<nav class="green lighten-2 nav-extended">
		<div class="nav-wrapper">
			<div class="container">
				<a href="#" class="brand-logo">BBS</a>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li><a href="index">帖子</a></li>
					<li><a href="rank">排行榜</a></li>
					<li><a href="user">我</a></li>
				</ul>
			</div>
			<a href="#" data-activates="slide-out" class="button-collapse"> <i
				class="material-icons">menu</i>
			</a>
		</div>
		<ul id="slide-out" class="side-nav">
			<li>
				<%
					User user = (User) session.getAttribute(User.CURRENT_USER);
					if (user != null) {
				%>
				<div class="userView">

					<div class="background">
						<img src="<%=user.getPicPath()%>" />
					</div>

					<a href="#!user"><img class="circle"
						src="<%=user.getPicPath()%>" /></a> <a href="#!name"><span
						class="waves-effect white-text name"><%=user.getUname()%></span></a>
					<a href="#!email"><span class="white-text smaller-font">这个人很懒，并没有留下什么..</span></a>
				</div> <%
					 	} else {
					 %>
				<div class="userView">

					<div class="background">
						<img src="imgs/default.png" />
					</div>

					<a href="login"><img class="circle"
						src="imgs/default.png" /></a> <a href="login"><span
						class="waves-effect white-text name">未登录</span></a>
					<a href="#!email"><span class="white-text smaller-font">请登录...</span></a>
				</div> <%
				 	}
				 %>
			</li>
			<li><a href="index" class="waves-effect">帖子</a></li>
			<li><a href="rank" class="waves-effect">排行榜</a></li>
			<li><a href="user" class="waves-effect">我</a></li>
		</ul>
			
	
		<a href="#" data-actives="slide-out" class="button-collapse"> <i
			class="material-icons">menu</i>
		</a>
	</nav>

</div>
