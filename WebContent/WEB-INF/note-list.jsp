<%@page import="top.soyask.bbs.domain.Gobal"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="top.soyask.bbs.domain.Note"%>
<%@page import="java.util.List"%>
<%@page import="top.soyask.bbs.domain.Page"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	List<Note> notes = (List<Note>) request.getAttribute("notes");
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	int type = (int) request.getAttribute("type");
	String data = (type == Gobal.NEW_NOTE) ? "new-items"
			: (type == Gobal.HOT_NOTE ? "hot-items" : "best-items");
%>
<%
	for (Note note : notes) {
%>
<div class="card-panel hoverable">
	<div class="row no-margin">
		<div class="col l1 m1 s2">
			<img src="<%=note.getuPicPath()%>"
				class="responsive-img circle head-tiny " />
		</div>
		<div class="col l9 m8 s10">
			<span class="truncate item-title-small"> <a
				href="note?mainId=<%=note.getMainId()%>"><%=note.getTitle()%></a>
			</span>
			<p class="grey-text"><%=note.getContent()%>
			</p>
			<div class="smaller-font grey-text text-darken-2">
				<i class="tiny material-icons ">visibility</i> (<%=note.getWatch()%>)
				<i class="tiny material-icons ">textsms</i> (<%=note.getReply()%>) <i
					class="tiny material-icons">thumb_up</i> (<%=note.getUp()%>) <i
					class="tiny material-icons">thumb_down</i> (<%=note.getDown()%>)
			</div>
		</div>
		<div class="col l2 m12 s12">

			<span class="right green-text smaller-font"><%=sdf.format(note.getCreateDate())%></span><br>
		</div>
	</div>
</div>

<%
	}
	Page pages = (Page) request.getAttribute("page");
	if (pages != null && pages.getCount() > 0) {
%>
<ul class="pagination">
	<li class="<%=pages.isFirst() ? "disabled" : "waves-effect"%>">
		<a class ="load" data-for="<%=data %>" href="next-note?page=1&noteType=<%=type%>">
			<i class="material-icons">chevron_left</i>
		</a>
	</li>
	<%
		List<Integer> p = pages.getRows();
			int current = pages.getCurrentPage();
			for (int str : p) {
	%>

	<li class="<%=str == current ? "active green lighten-1" : "waves-effect"%>">
		<a class ="load" data-for="<%=data %>" href="next-note?page=<%=str%>&noteType=<%=type%>"><%=str%></a>
	</li>
	<%
		}
	%>
	<li class="<%=pages.isLast() ? "disabled" : "waves-effect"%>">
	<a class ="load" data-for="<%=data %>" href="next-note?page=<%=pages.getCount()%>&noteType=<%=type%>"><i class="material-icons">chevron_right</i></a></li>
</ul>
<%
	}
%>
<script src="js/page.js"></script>
