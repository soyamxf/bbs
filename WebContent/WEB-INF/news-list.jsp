<%@page import="top.soyask.bbs.domain.Page"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="top.soyask.bbs.domain.Note"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<ul class="collapsible">
	<%
		List<Note> notes = (List<Note>) request.getAttribute("notes");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		for (Note note : notes) {
	%>
	<li>
		<div class="collapsible-header truncate">
			<a href="user?uid=<%=note.getUid()%>"> <img
				src="<%=note.getuPicPath()%>"
				class="responsive-img circle head-tiny" align="top" /> <%=note.getUname()%>
			</a>：<%=note.getTitle()%><span
				class="right hide-on-small-only new badge green" data-badge-caption><%=sdf.format(note.getCreateDate())%></span>
		</div>
		<div class="collapsible-body "><%=note.getContent()%></div>
	</li>

	<%
		}
	%>
</ul>
<script type="text/javascript">
	$('.collapsible').collapsible();
</script>
<%
	Page pages = (Page) request.getAttribute("page");
	if (pages != null && pages.getCount() > 0) {
%>
<ul class="pagination">
	<li class="<%=pages.isFirst() ? "disabled" : "waves-effect"%>">
		<a class ="load" data-for="news" href="user-new?page=1">
			<i class="material-icons">chevron_left</i>
		</a>
	</li>
	<%
		List<Integer> p = pages.getRows();
			int current = pages.getCurrentPage();
			for (int str : p) {
	%>

	<li class="<%=str == current ? "active green lighten-1" : "waves-effect"%>">
		<a class ="load" data-for="news" href="user-new?page=<%=str%>"><%=str%></a>
	</li>
	<%
		}
	%>
	<li class="<%=pages.isLast() ? "disabled" : "waves-effect"%>">
	<a class ="load" data-for="news" href="user-new?page=<%=pages.getCount()%>"><i class="material-icons">chevron_right</i></a></li>
</ul>
<%
	}
%>
<script src="js/page.js"></script>
