$(function() {
    $(".button-collapse").sideNav();
    $('#edit-dialog').modal();

    $("input[type='file']").change(function() {
        var reader = new FileReader();
        var file = event.target.files[0];
        reader.onload = function(e) {
            $("img#input-head").attr("src", e.target.result);
        }
        reader.readAsDataURL(file);
    })
    
    loadNextPage("user-new?page=1", "#news")
    loadNextPage("user-fans?page=1", "#fans")
    loadNextPage("user-follow?page=1", "#follow")

})
