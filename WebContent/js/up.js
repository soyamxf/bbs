$("#up").click(upOrDown);
$("#down").click(upOrDown);

function upOrDown(){
	var $this = this;
	$.post($(this).attr("href"),function(data){
		if (data.match("success") != null) {
			$($this).removeClass("grey");
			$($this).addClass("green");
		}
	}); 
	return false;
}