$(function(){
	$(".follow").click(function(){
		follow(this);
		return false;
	})
})

function follow(a){
	$.post($(a).attr("href"),function(data){
		if (data.match("success") != null) {
			var $i = $(a).find("i");
			var $text = $i.text();
			if($text.match("border")){
				$i.text("favorite");
			}else{
				$i.text("favorite_border");
			}
		}else if(data.match("error")!= null){
			Materialize.toast('关注失败', 4000)
		}else{
			location.href = "login";
		}
	}); 
}