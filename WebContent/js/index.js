$(function() {
    $(".button-collapse").sideNav();

    $(".edit-note").click(function() {
        $("#note-title").focus();
    });

    $(window).scroll(function() {
        var scrollTop = $(window).scrollTop();
        var htmlHeight = $("html").height();
        var height = $("nav").height();
        var footerHeight = $("footer").height();
        if (scrollTop < htmlHeight - footerHeight) {
            if (scrollTop > height) {
                $(".right-block").css("top", scrollTop - height);
            } else {
                $(".right-block").css("top", 0);
            }
        }
    });
    
    load()
});

function load(){
	loadNextPage("next-note?page=1&noteType=0", "#new-items")
    loadNextPage("next-note?page=1&noteType=1", "#hot-items")
    loadNextPage("next-note?page=1&noteType=2", "#best-items")
}
