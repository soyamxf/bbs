function next() {
	var $as = $("a.load");
	for (var i = 0; i < $as.length; i++) {
		$($as[i]).click(function() {
			var $url = $(this).attr("href");
			var $target = "#" + $(this).attr("data-for");
			loadNextPage($url, $target);
			return false;
		});
	}
}
next();