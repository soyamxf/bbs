$(function() {
	$("form").submit(function() {
		var $action = $(this).attr("action");
		$.post($action, $(this).serialize(), function(data, status) {
			if (data.match("success") != null) {
				load();
				Materialize.toast("发送成功", 2000);
				document.getElementsByTagName("form")[0].reset();
			}else if(data.match("error") != null){
				location.href="login";
			}
		})
		return false;
	});
})