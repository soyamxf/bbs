package top.soyask.db.factory;

import java.util.HashMap;
import java.util.Map;

import top.soyask.db.orm.SQLCreator;

public class SQLCreatorFactory {

	public static SQLCreatorFactory factory = new SQLCreatorFactory();
	
	private Map<Class, SQLCreator> map = new HashMap<>();

	private SQLCreatorFactory() {
	}

	public synchronized <T, PK> SQLCreator<T, PK> getSQLCreator(Class<T> clazz) {
		if (map.containsKey(clazz)) {
			return map.get(clazz);
		}
		SQLCreator<T, PK> creator = new SQLCreator<>(clazz);
		map.put(clazz, creator);
		return creator;
	}

	public static SQLCreatorFactory getInstence() {
		return factory;
	}
}
