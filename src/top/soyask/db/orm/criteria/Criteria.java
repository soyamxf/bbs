package top.soyask.db.orm.criteria;

public class Criteria {

	private String key;
	private Object value = "null";
	private String symbol = "=";

	public Criteria() {
	}

	public Criteria(String key, Object value) {
		this.key = key;
		setValue(value);
	}

	public Criteria(String key, String symbol, Object value) {
		this.key = key;
		this.symbol = symbol;
		setValue(value);
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		if (value != null) {
			this.value = "'" + value.toString() + "'";
		}
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

}
