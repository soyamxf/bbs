package top.soyask.db.orm;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import top.soyask.db.annotation.Igonre;
import top.soyask.db.annotation.PrimaryKey;
import top.soyask.db.exception.PrimaryKeyException;

/**
 * 
 * 该类用来解析一个对象，获取它的字段值和方法。
 * <p>
 * Create on 2017年4月14日
 *
 * @param <T>
 *            该对象的所要解析的对象
 * @param <PK>
 *            主键
 * 
 * @author mxf
 */
public class ObjectResolver<T, PK> {
	private Logger logger = Logger.getLogger(getClass().getName());

	private Class<T> target;
	private String primaryKey;
	private List<String> fieldNames;
	private Map<String, Type> fieldTypes;
	private Map<String, Method> setters;
	private Map<String, Method> getters;

	public ObjectResolver(Class<T> clazz) {
		this.target = clazz;
		init();
	}

	public void init() {
		initFields();
		initMethods();
	}

	private void initMethods() {
		setters = new HashMap<>();
		getters = new HashMap<>();

		Method[] methods = getMethods(target);
		for (Method method : methods) {
			String name = method.getName();
			String methodName = name.length() > 3 ? 
					name.substring(3, 4).toLowerCase() + name.substring(4) : "";

			if (fieldNames.contains(methodName) || methodName.equals(primaryKey)) {
				if (name.startsWith("set")) {
					setters.put(methodName, method);
				} else if (name.startsWith("get") && !"getClass".equals(name)) {
					getters.put(methodName, method);
				}
			}
		}
	}

	private void initFields() {
		Field[] fields = getFields(target);
		fieldNames = new ArrayList<>();
		fieldTypes = new HashMap<>();

		for (Field field : fields) {
			if (!isIgnore(field) && !initPk(field)) {
				fieldNames.add(field.getName());
				fieldTypes.put(field.getName(), field.getType());
			}
		}
	}

	public boolean isIgnore(Field field) {
		Igonre igonre = field.getAnnotation(Igonre.class);
		return igonre != null;
	}

	public void doSet(Object obj, String methodName, Object... params)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Method method = setters.get(methodName);
		if (method != null) {
			method.invoke(obj, params);
		}
	}

	@SuppressWarnings("unchecked")
	public <E> E doGet(Object obj, String methodName) {
		Method method = getters.get(methodName);
		Object invoke = null;
		try {
			invoke = method.invoke(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return (E) invoke;
	}

	public Object[] getFieldValues(Object o) {
		Object[] values = new Object[fieldNames.size()];
		for (int i = 0; i < fieldNames.size(); i++) {
			Object obj = doGet(o, fieldNames.get(i));
			values[i] = obj;
		}
		return values;
	}

	private boolean initPk(Field field) {
		PrimaryKey annotation = field.getAnnotation(PrimaryKey.class);
		if (annotation != null) {
			if (primaryKey == null) {
				primaryKey = field.getName();
				fieldTypes.put(primaryKey, field.getType());
				return true;
			} else {
				throw new PrimaryKeyException();
			}
		}
		return false;
	}

	private Field[] getFields(Class<T> clazz) {
		Field.setAccessible(clazz.getDeclaredFields(), false);
		Field[] fields = clazz.getDeclaredFields();
		return fields;
	}

	private Method[] getMethods(Class<T> clazz) {
		Method.setAccessible(clazz.getDeclaredMethods(), false);
		Method[] methods = clazz.getDeclaredMethods();
		return methods;
	}

	public List<String> getFieldNames() {
		return fieldNames;
	}

	public String getPrimaryKey() {
		return primaryKey;
	}

	public String getTableName() {
		String name = target.getName();
		String[] split = name.split("\\.");
		String tableName = split[split.length - 1];
		return tableName;
	}

	public T newObject(Map<String, Object> map) throws Exception {
		T t = target.newInstance();
		Set<String> keySet = map.keySet();
		for (String fieldName : keySet) {
			doSet(t, fieldName, map.get(fieldName));
		}
		return t;
	}

	public Type getFieldType(String fieldName) {
		return fieldTypes.get(fieldName);
	}
}
