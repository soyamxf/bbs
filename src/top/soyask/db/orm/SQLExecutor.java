package top.soyask.db.orm;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.mysql.jdbc.Statement;

/**
 * 
 * 用来执行sql语言
 * <p>
 * 
 * Create on 2017年4月14日
 * 
 * @author mxf
 */
public class SQLExecutor {

	private static String username = "";
	private static String password = "";
	private static String url = "";
	private static String driver = "";

	private static Properties properties;
	private static InputStream stream;

	static {
		try {
			properties = new Properties();
			stream = SQLExecutor.class.getClassLoader().getResourceAsStream("easier-mysql.properties");
			properties.load(stream);

			url = properties.getProperty("url");
			username = properties.getProperty("username");
			password = properties.getProperty("password");
			driver = properties.getProperty("driver");

			Class.forName(driver);

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			stream = null;
			properties = null;
			driver = null;
		}
	}

	private Connection getConnection() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(url, username, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}

	/**
	 * 执行带参数的sql语句
	 * 
	 * @param sql
	 *            执行的sql语句
	 * @param parameters
	 *            参数
	 * @return 执行语句结束后的每条记录封装成Object[]数组 并加入到List集合中
	 */

	public List<Map<String, Object>> executeQuery(String sql,List<String> fieldNames,String primaryKeyName, Object... parameters) {
		List<Map<String, Object>> list = new ArrayList<>();
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = getConnection();
			statement = connection.prepareStatement(sql);
			if (parameters != null && parameters.length > 0) {
				for (int i = 0; i < parameters.length; i++) {
					statement.setObject(i + 1, parameters[i]);
				}
			}
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Map<String, Object> map = new HashMap<>();
				for (String fieldName : fieldNames) {
					map.put(fieldName, resultSet.getObject(fieldName));
				}
				map.put(primaryKeyName, resultSet.getObject(primaryKeyName));
				list.add(map);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("executeQuery方法出现错误");
		} finally {
			close(resultSet, statement, connection);
		}
		return list;
	}
	

	public List<Map<String,Object>> executeSQL(String sql, Object...parameters) {

		List<Map<String,Object>> list = new ArrayList<>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {

			connection = getConnection();
			statement = connection.prepareStatement(sql);
			if (parameters != null && parameters.length > 0) {
				for (int i = 0; i < parameters.length; i++) {
					statement.setObject(i + 1, parameters[i]);
				}
			}
			resultSet = statement.executeQuery();
			List<String> columnNames = new ArrayList<>();
			ResultSetMetaData metaData = resultSet.getMetaData();
			int columnCount = metaData.getColumnCount();
			
			for (int i = 1; i <= columnCount; i++) {
				String columnName = metaData.getColumnName(i);
				columnNames.add(columnName);
			}
			
			while (resultSet.next()) {
				Map<String,Object> map = new HashMap<>();
				for (String name : columnNames) {
					map.put(name, resultSet.getObject(name));
				}
				list.add(map);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("executeQuery方法出现错误");
		} finally {
			close(resultSet,statement,connection);
		}
		
		return list;
	}
	/**
	 * 执行没有返回值的sql语句
	 * 
	 * @param sql
	 *            要执行的sql语句
	 * @param parameters
	 *            参数
	 */

	public void executeUpdate(String sql, Object... parameters) {

		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = getConnection();
			statement = execute(sql, connection, parameters);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		} finally {
			close(null, statement, connection); // 关闭
		}
	}

	/**
	 * 执行插入语句 并返回主键
	 * 
	 * @param sql
	 *            要执行的sql语句
	 * @param parameters
	 *            参数
	 * @return 插入得到的主键值
	 */
	public Object executeInsert(String sql, Object... parameters) {

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet;
		try {
			connection = getConnection();
			statement = execute(sql, connection, parameters);
			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getObject(1);
			}
			return -1;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		} finally {
			close(null, statement, connection); // 关闭
		}
	}

	private PreparedStatement execute(String sql, Connection connection, Object... parameters) throws SQLException {
		PreparedStatement statement;
		statement = connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);

		if (parameters != null && parameters.length > 0) {
			for (int i = 0; i < parameters.length; i++) {
				statement.setObject(i + 1, parameters[i]);
			}
		}
		statement.executeUpdate();
		return statement;
	}

	private void close(ResultSet resultSet, PreparedStatement statement, Connection connection) {
		try {
			if (resultSet != null) {
				resultSet.close();
				resultSet = null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (statement != null) {
					statement.close();
					statement = null;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					if (connection != null) {
						connection.close();
						connection = null;
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
