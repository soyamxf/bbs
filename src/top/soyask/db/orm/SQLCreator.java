package top.soyask.db.orm;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.sun.istack.internal.NotNull;

import top.soyask.bbs.domain.Gobal;
import top.soyask.db.orm.criteria.Criteria;
import top.soyask.db.orm.criteria.Order;
import top.soyask.db.utils.DBDataType;

/**
 * 根据解析好的对象来创建SQL语句 Create on 2017年4月15日
 * 
 * @param <T>
 *            要持久化的对象
 * @param <PK>
 *            主键
 * @author mxf
 * 
 */
public class SQLCreator<T, PK> {

	private ObjectResolver<T, PK> resolver;
	private SQLExecutor executor;
	private Logger logger = Logger.getLogger(getClass().getSimpleName());

	public SQLCreator(Class<T> clazz) {
		resolver = new ObjectResolver<>(clazz);
		executor = new SQLExecutor();
	}

	public void createTable() {
		StringBuffer buffer = new StringBuffer();
		List<String> fieldNames = resolver.getFieldNames();
		buffer.append("create table ");
		buffer.append(resolver.getTableName());
		buffer.append(" (");
		buffer.append("");
		for (String field : fieldNames) {
			Type type = resolver.getFieldType(field);
			String dataType = DBDataType.getDataType(type);
			buffer.append(field);
			buffer.append(" " + dataType);
			buffer.append(",");
		}
		String primaryKey = resolver.getPrimaryKey();
		if(primaryKey != null){
			Type fieldType = resolver.getFieldType(primaryKey);
			buffer.append(primaryKey);
			buffer.append(" " + DBDataType.getDataType(fieldType));
			buffer.append(" PRIMARY KEY AUTO_INCREMENT");
		}else {
			buffer.deleteCharAt(buffer.length()-1);
		}
	
		buffer.append(")");
		executor.executeUpdate(buffer.toString());
	}

	@SuppressWarnings("unchecked")
	public PK insert(Object obj) {
		List<String> fieldNames = resolver.getFieldNames();
		int size = fieldNames.size();

		StringBuffer buffer = new StringBuffer();
		buffer.append("insert into ");
		buffer.append(resolver.getTableName());
		buffer.append("(");
		for (int i = 0; i < size - 1; i++) {
			buffer.append(fieldNames.get(i) + ", ");
		}
		buffer.append(fieldNames.get(size - 1) + ") values (");
		for (int i = 0; i < size - 1; i++) {
			buffer.append("?,");
		}
		buffer.append("?)");
		if(Gobal.out){
			logger.info(buffer.toString());
		}

		// Object[] params = new Object[size];
		// for (int i = 0; i < params.length; i++) {
		// try {
		// params[i] = resolver.doGet(obj, fieldNames.get(i));
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		// }
		Object pk = executor.executeInsert(buffer.toString(), resolver.getFieldValues(obj));
		return (PK) pk;
	}

	public boolean deleteObject(Object t) {
		try {
			String primaryKey = resolver.getPrimaryKey();
			PK pk = resolver.doGet(t, primaryKey);
			return deleteByKey(pk);
		} catch (Exception e) {
			return false;
		}
	}

	public boolean delete(Criteria... criterias) {

		StringBuffer buffer = new StringBuffer();
		buffer.append("delete from ");
		buffer.append(resolver.getTableName());
		if (criterias != null && criterias.length > 0) {
			buffer.append(" where ");
			for (int i = 0; i < criterias.length - 1; i++) {
				buffer.append(criterias[i].getKey() + " ");
				buffer.append(criterias[i].getSymbol());
				buffer.append(" " + criterias[i].getValue());
				buffer.append(" and ");
			}
			buffer.append(criterias[criterias.length - 1].getKey() + " ");
			buffer.append(criterias[criterias.length - 1].getSymbol());
			buffer.append(" " + criterias[criterias.length - 1].getValue());
		}
		try {
			executor.executeUpdate(buffer.toString());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean deleteByKey(Object pk) {
		try {
			StringBuffer buffer = new StringBuffer();
			buffer.append("delete from ");
			buffer.append(resolver.getTableName());
			buffer.append(" where ");
			buffer.append(resolver.getPrimaryKey());
			buffer.append(" = ?");
			executor.executeUpdate(buffer.toString(), pk);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public T get(PK pk) throws Exception {
		StringBuffer buffer = new StringBuffer();
		buffer.append("select * from ");
		buffer.append(resolver.getTableName());
		buffer.append(" where ");
		buffer.append(resolver.getPrimaryKey());
		buffer.append(" = ?");
		List<Map<String, Object>> list = executor.executeQuery(buffer.toString(), resolver.getFieldNames(),
				resolver.getPrimaryKey(), pk);
		return list.isEmpty() ? null : resolver.newObject(list.get(0));
	}

	public List<T> getByCriteria(Criteria... criterias) throws Exception {
		StringBuffer buffer = new StringBuffer();
		buffer.append("select * from ");
		buffer.append(resolver.getTableName());
		if (criterias != null && criterias.length > 0) {
			buffer.append(" where ");
			for (int i = 0; i < criterias.length - 1; i++) {
				buffer.append(criterias[i].getKey() + " ");
				buffer.append(criterias[i].getSymbol());
				buffer.append(" " + criterias[i].getValue());
				buffer.append(" and ");
			}
			buffer.append(criterias[criterias.length - 1].getKey() + " ");
			buffer.append(criterias[criterias.length - 1].getSymbol());
			buffer.append(" " + criterias[criterias.length - 1].getValue());
		}
		// List<Map<String, Object>> list =
		// executor.executeQuery(buffer.toString(), resolver.getFieldNames());
		// List<T> result = map2Obj(list);
		return select(buffer.toString());
	}

	public List<T> next(int start, int count, Criteria... criterias) throws Exception {
		return next(start, count, null, criterias);
	}

	public List<T> next(int start, int count, Order order, Criteria... criterias) throws Exception {
		StringBuffer buffer = new StringBuffer();
		buffer.append("select * from ");
		buffer.append(resolver.getTableName());
		if (criterias != null && criterias.length > 0) {
			buffer.append(" where ");
			for (int i = 0; i < criterias.length - 1; i++) {
				buffer.append(criterias[i].getKey() + " ");
				buffer.append(criterias[i].getSymbol());
				buffer.append(" " + criterias[i].getValue());
				buffer.append(" and ");
			}
			buffer.append(criterias[criterias.length - 1].getKey() + " ");
			buffer.append(criterias[criterias.length - 1].getSymbol());
			buffer.append(" " + criterias[criterias.length - 1].getValue());
		}
		if (order != null) {
			buffer.append(" order by " + order.getColunm());
			buffer.append(order.isAsc() ? " asc" : " desc");
		}
		buffer.append(" limit ?, ?");
		List<Map<String, Object>> list = executor.executeQuery(buffer.toString(), resolver.getFieldNames(),
				resolver.getPrimaryKey(), start, count);
		return map2Obj(list);
	}

	public List<T> next(int start, int count) throws Exception {
		// StringBuffer buffer = new StringBuffer();
		// buffer.append("select * from ");
		// buffer.append(resolver.getTableName());
		// buffer.append(" limit ?, ?");
		// System.out.println(buffer.toString());
		// List<Map<String, Object>> list =
		// executor.executeQuery(buffer.toString(), resolver.getFieldNames(),
		// start,
		// count);
		// return map2Obj(list);
		return next(start, count, null);
	}

	public List<T> getAll() throws Exception {
		StringBuffer buffer = new StringBuffer();
		buffer.append("select * from ");
		buffer.append(resolver.getTableName());
		// List<Map<String, Object>> list =
		// executor.executeQuery(buffer.toString(), resolver.getFieldNames());
		// List<T> result = map2Obj(list);
		return select(buffer.toString());
	}

	public List<T> select(String sql) throws Exception {
		List<Map<String, Object>> list = executor.executeQuery(sql, resolver.getFieldNames(), resolver.getPrimaryKey());
		return map2Obj(list);
	}

	public boolean update(Object obj) {
		Object pk = resolver.doGet(obj, resolver.getPrimaryKey());
		if (pk == null) {
			throw new NullPointerException("无法更新，主键不能为空");
		}
		List<String> fieldNames = resolver.getFieldNames();
		StringBuffer buffer = new StringBuffer();
		buffer.append("update ");
		buffer.append(resolver.getTableName());
		buffer.append(" set ");
		int size = fieldNames.size();
		for (int i = 0; i < size - 1; i++) {
			buffer.append(fieldNames.get(i));
			buffer.append(" = ?, ");
		}
		buffer.append(fieldNames.get(size - 1));
		buffer.append(" = ?");
		buffer.append(" where ");
		buffer.append(resolver.getPrimaryKey());
		buffer.append(" = ?");
		Object[] fieldValues = resolver.getFieldValues(obj);
		Object[] values = Arrays.copyOf(fieldValues, fieldValues.length + 1);
		values[fieldValues.length] = pk;
		executor.executeUpdate(buffer.toString(), values);
		return true;
	}

	private List<T> map2Obj(List<Map<String, Object>> list) throws Exception {
		List<T> result = new ArrayList<>();
		for (Map<String, Object> map : list) {
			T t = resolver.newObject(map);
			result.add(t);
		}
		return result;
	}
	
	public long count() {
		String sql = "select count(*) from " + resolver.getTableName();
		List<Map<String, Object>> result = executor.executeSQL(sql);
		if (result.size() > 0) {
			Map<String, Object> objects = result.get(0);
			return (long) objects.get("count(*)");
		}
		return 0;
	}
	

	public long count(@NotNull Criteria...criterias) {
		StringBuffer sql = new StringBuffer("select count(*) from " + resolver.getTableName());
		if(criterias != null){
			sql.append(" where ");
			if(criterias != null){
				for (Criteria criteria : criterias) {
					sql.append(criteria.getKey()+criteria.getSymbol()+criteria.getValue());
					sql.append(" and ");
				}
				sql.append("true");
			}
		}
		List<Map<String, Object>> result = executor.executeSQL(sql.toString());
		if (result.size() > 0) {
			Map<String, Object> objects = result.get(0);
			return (long) objects.get("count(*)");
		}
		return 0;
	}

	public List<Map<String, Object>> customSQL(String sql, Object... parameters) {

		return executor.executeSQL(sql, parameters);
	}
	
	public void customUpdate(String sql, Object... parameters) {
		 executor.executeUpdate(sql, parameters);
	}
}
