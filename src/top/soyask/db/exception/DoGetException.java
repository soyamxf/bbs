package top.soyask.db.exception;

public class DoGetException extends RuntimeException {
	private static final long serialVersionUID = -7720660872332214309L;

	public DoGetException() {
		super("doGet 方法错误，请检查该对象是否有该getter 方法");
	}
}
