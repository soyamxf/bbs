package top.soyask.db.exception;

/**
 * 约定每个可持久化对象都只有一个主键， 否则，抛出该异常。
 * <p>
 * Create on 2017年4月14日
 *
 * @author mxf
 */
public class PrimaryKeyException extends RuntimeException {

	private static final long serialVersionUID = 3190316278986117872L;

	public PrimaryKeyException() {
		super("不要定义多个主键  这代码吃不消...2333");
	}
}
