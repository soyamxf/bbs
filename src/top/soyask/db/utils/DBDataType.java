package top.soyask.db.utils;

import java.lang.reflect.Type;

/**
 * 根据Field的字段类型，获取相应表中字段的类型。
 * <p>
 * Create on 2017年4月15日
 * 
 * @author mxf
 */
public class DBDataType {

	public static String getDataType(Type clazz) {
		switch (clazz.getTypeName()) {
		case "java.lang.Integer":
		case "int":
			return "INTEGER";
		case "java.lang.Boolean":
		case "boolean":
			return "TINYINT";
		case "java.util.Date":
		case "java.sql.Date":
			return "DATETIME";
		case "java.lang.Float":
		case "float":
			return "FLOAT";
		case "java.lang.Double":
		case "double":
			return "DOUBLE";
		case "java.lang.Long":
		case "long":
			return "BIGINT";
		case "java.lang.String":
			return "VARCHAR(120)";
		default:
		}

		return "";
	}
}
