package top.soyask.db.operator;

import java.util.List;
import java.util.Map;

import top.soyask.db.factory.SQLCreatorFactory;
import top.soyask.db.orm.SQLCreator;
import top.soyask.db.orm.criteria.Criteria;
import top.soyask.db.orm.criteria.Order;

public class ObjectOperator {

	private SQLCreatorFactory factory = SQLCreatorFactory.getInstence();

	public <PK> PK save(Object obj) {
		SQLCreator<?, PK> sqlCreator = factory.getSQLCreator(obj.getClass());
		return sqlCreator.insert(obj);
	}

	public <E> void createTable(Class<E> clazz) {
		SQLCreator<?, Object> sqlCreator = factory.getSQLCreator(clazz);
		sqlCreator.createTable();
	}

	public <E> boolean delete(Class<E> clazz, Criteria... criterias) {
		SQLCreator<E, ?> sqlCreator = factory.getSQLCreator(clazz);
		return sqlCreator.delete(criterias);
	}

	public <PK, E> boolean deleteByKey(Class<E> clazz, PK pk) {
		SQLCreator<?, PK> sqlCreator = factory.getSQLCreator(clazz);
		return sqlCreator.deleteByKey(pk);
	}

	public boolean deleteObject(Object obj) {
		SQLCreator<? extends Object, ?> sqlCreator = factory.getSQLCreator(obj.getClass());
		return sqlCreator.deleteObject(obj);
	}

	public <E, PK> E get(Class<E> clazz, PK pk) throws Exception {
		SQLCreator<E, PK> sqlCreator = factory.getSQLCreator(clazz);
		return sqlCreator.get(pk);
	}

	public <E> List<E> getAll(Class<E> clazz) throws Exception {
		SQLCreator<E, ?> sqlCreator = factory.getSQLCreator(clazz);
		return (List<E>) sqlCreator.getAll();
	}

	public <E> List<E> getByCriteria(Class<E> clazz, Criteria... criterias) throws Exception {
		SQLCreator<E, ?> sqlCreator = factory.getSQLCreator(clazz);
		return sqlCreator.getByCriteria(criterias);
	}

	public <E> List<E> next(Class<E> clazz, int start, int count) throws Exception {
		SQLCreator<E, ?> sqlCreator = factory.getSQLCreator(clazz);
		return sqlCreator.next(start, count);
	}

	public <E> List<E> next(Class<E> clazz, int start, int count, Criteria... criterias) throws Exception {
		SQLCreator<E, ?> sqlCreator = factory.getSQLCreator(clazz);
		return sqlCreator.next(start, count, criterias);
	}

	public <E> List<E> next(Class<E> clazz, int start, int count, Order order, Criteria... criterias) throws Exception {
		SQLCreator<E, ?> sqlCreator = factory.getSQLCreator(clazz);
		return sqlCreator.next(start, count, order, criterias);
	}

	public boolean update(Object obj) {
		SQLCreator<?, ?> sqlCreator = factory.getSQLCreator(obj.getClass());
		return sqlCreator.update(obj);
	}

	public <E> List<E> select(Class<E> clazz, String sql) throws Exception {
		SQLCreator<E, ?> sqlCreator = factory.getSQLCreator(clazz);
		return sqlCreator.select(sql);
	}

	public <E> long count(Class<E> clazz) {
		SQLCreator<E, ?> sqlCreator = factory.getSQLCreator(clazz);
		return sqlCreator.count();
	}
	
	public <E> long count(Class<E> clazz,Criteria...criterias) {
		SQLCreator<E, ?> sqlCreator = factory.getSQLCreator(clazz);
		return sqlCreator.count(criterias);
	}

	public List<Map<String, Object>> executeSQL(String sql, Object... params) {
		SQLCreator<?, ?> sqlCreator = factory.getSQLCreator(Object.class);
		return sqlCreator.customSQL(sql, params);
	}
	
	public void executeUpdate(String sql, Object... params) {
		SQLCreator<?, ?> sqlCreator = factory.getSQLCreator(Object.class);
		sqlCreator.customUpdate(sql, params);
	}
}
