package top.soyask.db.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 用来标识可持久化对象忽略的属性。
 * 在Field中使用。
 * <p>
 * Create on 2017年4月14日
 * 
 * @author mxf
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Igonre {

}
