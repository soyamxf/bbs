package top.soyask.db.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * 标识可持久化对象中的主键 约定一个对象中仅有一个，否则将无法正常使用该系列代码。
 * 至于为什么这样规定，是因为这样写省力。
 * <p>
 * Create on 2017年4月14日 
 * @author mxf
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PrimaryKey {

}
