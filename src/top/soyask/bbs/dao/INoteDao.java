package top.soyask.bbs.dao;

import java.util.List;
import java.util.Map;

import top.soyask.bbs.domain.Note;

public interface INoteDao {

	List<Note> nextNewNotes(long uid, int page, int itemCount, int type);

	List<Note> nextNewNotes(int page, int itemCount);

	List<Note> nextHotNotes(int page, int itemCount);

	List<Note> nextBestNotes(int page, int itemCount);

	long count(int type);

	long count(long uid);

	long subCount(long mainId);

	long create(Note note);
	
	void update(Note note);

	List<Map<String, Object>> nextSubNotes(long mainId, int page, int itemCount);

	void watch(long mainId);

	int getReply(long mainId);
	
	Note getNote(long mainId);

	void up(long nid);
	
	void down(long nid);

}
