package top.soyask.bbs.dao.impl;

import java.util.List;
import java.util.Map;

import top.soyask.bbs.dao.INoteDao;
import top.soyask.bbs.domain.Gobal;
import top.soyask.bbs.domain.Note;
import top.soyask.db.operator.ObjectOperator;
import top.soyask.db.orm.criteria.Criteria;
import top.soyask.db.orm.criteria.Order;

public class NoteDao implements INoteDao {

	private ObjectOperator operator = new ObjectOperator();

	@Override
	public List<Note> nextNewNotes(long uid, int page, int itemCount, int type) {
		Order order = new Order(false, "createDate");
		Criteria criteria = new Criteria("type", 1);
		Criteria criteria0 = new Criteria("uid", uid);
		final int start = (page - 1) * itemCount;
		List<Note> notes = null;
		try {
			notes = operator.next(Note.class, start, itemCount, order, criteria0, criteria);
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return notes;
	}

	@Override
	public List<Note> nextNewNotes(int page, int itemCount) {
		Order order = new Order(false, "createDate");
		Criteria criteria = new Criteria("type", 1);
		List<Note> notes = nextNotes(page, itemCount, order, criteria);
		return notes;
	}

	@Override
	public List<Note> nextHotNotes(int page, int itemCount) {
		Order order = new Order("reply");
		Criteria criteria = new Criteria("type", 1);
		List<Note> notes = nextNotes(page, itemCount, order, criteria);
		return notes;
	}

	@Override
	public List<Note> nextBestNotes(int page, int itemCount) {
		Order order = new Order(false, "createDate");
		Criteria criteria = new Criteria("type", 1);
		Criteria criteria0 = new Criteria("reply", ">", 100);
		List<Note> notes = nextNotes(page, itemCount, order, criteria, criteria0);
		return notes;
	}

	private List<Note> nextNotes(int page, int itemCount, Order order, Criteria... criteria) {
		final int start = (page - 1) * itemCount;
		List<Note> notes = null;
		try {
			notes = operator.next(Note.class, start, itemCount, order, criteria);
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return notes;
	}

	@Override
	public long count(int type) {
		long count = 0;
		Criteria c = new Criteria("type", "1");
		if (type == Gobal.BEST_NOTE) {
			Criteria criteria = new Criteria("reply", ">", 100);
			count = operator.count(Note.class, c, criteria);
		} else {
			count = operator.count(Note.class, c);
		}
		return count;
	}

	@Override
	public long count(long uid) {
		long count = 0;
		Criteria c = new Criteria("type", "1");
		Criteria criteria = new Criteria("uid", uid);
		count = operator.count(Note.class, c, criteria);
		return count;
	}

	@Override
	public long create(Note note) {
		return operator.save(note);
	}

	@Override
	public long subCount(long mainId) {
		long count = 0;
		Criteria criteria = new Criteria("mainId", mainId);
		count = operator.count(Note.class, criteria);
		return count;
	}

	@Override
	public List<Map<String, Object>> nextSubNotes(long mainId, int page, int itemCount) {
		final int start = (page - 1) * itemCount;
		String sql = "select * from note join user on " +
				"note.uid = user.uid where mainId = ? order by floor limit ?,? ";
		List<Map<String, Object>> result = operator.executeSQL(sql, mainId, start, itemCount);
		return result;
	}

	@Override
	public void watch(long mainId) {
		String sql = "update note set watch = watch+1 where mainId = ? and type = 1";
		operator.executeUpdate(sql, mainId);
	}

	@Override
	public int getReply(long mainId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Note getNote(long mainId) {
		Criteria criteria = new Criteria("mainId", mainId);
		Criteria c = new Criteria("type", 1);
		Note note = null;
		try {
			List<Note> notes = operator.getByCriteria(Note.class, criteria, c);
			if (!notes.isEmpty()) {
				note = notes.get(0);
			}
		} catch (Exception e) {

		}
		return note;
	}

	@Override
	public void update(Note note) {
		operator.update(note);
	}

	@Override
	public void up(long mainId) {
		String sql = "update note set up = up+1 where mainId = ? and type = 1";
		operator.executeUpdate(sql, mainId);		
	}
	@Override
	public void down(long mainId) {
		String sql = "update note set down = down+1 where mainId = ? and type = 1";
		operator.executeUpdate(sql, mainId);		
	}
}
