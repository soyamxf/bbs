package top.soyask.bbs.dao.impl;

import java.util.List;

import top.soyask.bbs.dao.IUserDao;
import top.soyask.bbs.domain.User;
import top.soyask.db.operator.ObjectOperator;
import top.soyask.db.orm.criteria.Criteria;
import top.soyask.db.orm.criteria.Order;

public class UserDao implements IUserDao {
	private ObjectOperator operator = new ObjectOperator();

	@Override
	public void save(User user) {
		operator.save(user);
	}

	@Override
	public void delete(User user) {
		operator.deleteObject(user);
	}

	@Override
	public void delete(Long id) {
		operator.deleteByKey(Long.class, id);
	}

	@Override
	public void validate(User user) {
	}

	@Override
	public List<User> next() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User get(long uid) {
		User user = null;
		try {
			user = operator.get(User.class, uid);
		} catch (Exception e) {
			e.printStackTrace();
			return null;

		}
		return user;
	}

	@Override
	public List<User> nextUserByExp(int page, int itemCount) {
		Order order = new Order(false, "exp");
		List<User> users = null;
		try {
			users = operator.next(User.class, (page - 1) * itemCount, itemCount, order);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}

	@Override
	public boolean login(long uid, String password) {
		Criteria criteria = new Criteria("uid", uid);
		Criteria criteria0 = new Criteria("password", password);
		List<User> list = null;
		try {
			list = operator.getByCriteria(User.class, criteria, criteria0);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return list != null && !list.isEmpty();
	}

	@Override
	public void update(User user) {
		operator.update(user);
	}

	@Override
	public long count() {
		long count = operator.count(User.class);
		return count;
	}

}
