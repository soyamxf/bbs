package top.soyask.bbs.dao;

import java.util.List;

import top.soyask.bbs.domain.User;

public interface IUserDao {
	void save(User user);
	void delete(User user);
	void delete(Long id);
	void validate(User user);
	List<User> next();
	User get(long uid);
	List<User> nextUserByExp(int page, int itemCount);
	boolean login(long uid, String password);
	void update(User user);
	long count();
}
