package top.soyask.bbs.dao;

import java.util.List;

import top.soyask.bbs.domain.Fans;
import top.soyask.bbs.domain.User;

public interface IFansDao {
	void follow(Fans fans, boolean follow);

	List<User> nextFans(long uid, int page, int itemCount);

	List<User> nextFollow(long uid, int page, int itemCount);

	long fansCount(long uid);
	
	long followCount(long uid);
	
	List<Long> followId(long uid);

}
