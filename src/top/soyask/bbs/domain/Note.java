package top.soyask.bbs.domain;

import java.util.Date;

import top.soyask.db.annotation.PrimaryKey;
/**
 * <p>	
 *	Create on 2017年4月24日
 * @author mxf
 *
 */
public class Note {
	
	@PrimaryKey
	private long nid;
	private long mainId; //帖子主id
	private long uid;//发帖者id
	private int floor; //帖子楼层
	private int up;
	private int down;
	private int watch;
	private int reply;
	private int type; //帖子类型 主题帖 回帖
	private String title; 
	private String content;
	private Date createDate;
	private String uPicPath;
	private String uname;

	public long getNid() {
		return nid;
	}

	public void setNid(long nid) {
		this.nid = nid;
	}

	public long getMainId() {
		return mainId;
	}

	public void setMainId(long mainId) {
		this.mainId = mainId;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public int getFloor() {
		return floor;
	}

	public void setFloor(int floor) {
		this.floor = floor;
	}

	public int getUp() {
		return up;
	}

	public void setUp(int up) {
		this.up = up;
	}

	public int getDown() {
		return down;
	}

	public void setDown(int down) {
		this.down = down;
	}

	public int getWatch() {
		return watch;
	}

	public void setWatch(int watch) {
		this.watch = watch;
	}

	public int getReply() {
		return reply;
	}

	public void setReply(int reply) {
		this.reply = reply;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}
	
	public String getContentCut(int size) {
		if(content.length() > size){
			return content.substring(0, size)+"...";
		}
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getuPicPath() {
		return uPicPath;
	}

	public void setuPicPath(String uPicPath) {
		this.uPicPath = uPicPath;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}
	
	public void setUser(User user){
		setUname(user.getUname());
		setUid(user.getUid());
		setuPicPath(user.getPicPath());
	}

}
