package top.soyask.bbs.domain;

import top.soyask.db.annotation.Igonre;
import top.soyask.db.annotation.PrimaryKey;

/**
 *
 * <p>
 * Create on 2017��4��24��
 * 
 * @author mxf
 *
 */
public class User {
	
	@Igonre
	public static final String CURRENT_USER = "current_user";
	@Igonre
	public static final String FOLLOW = "follow";
	
	@PrimaryKey
	private long uid;
	private String uname;
	private String password;
	private String picPath;
	private int exp;
	private String info;
	private int sex;
	private int fans;
	private int follow;

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPicPath() {
		if(picPath == null){
			return "imgs/default.png";
		}
		return picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public int getExp() {
		return exp;
	}

	public void setExp(int exp) {
		this.exp = exp;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public int getFans() {
		return fans;
	}

	public void setFans(int fans) {
		this.fans = fans;
	}

	public int getFollow() {
		return follow;
	}

	public void setFollow(int follow) {
		this.follow = follow;
	}
}
