package top.soyask.bbs.domain;

import java.util.List;

/**
 *
 * <p>
 * Create on 2017��4��24��
 * 
 * @author mxf
 *
 */
public class Page {

	public static final int ITEM_COUNT = 10;

	private int count;
	private List<Integer> rows;
	private int currentPage;
	private boolean first;
	private boolean last;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<Integer> getRows() {
		return rows;
	}

	public void setRows(List<Integer> rows) {
		this.rows = rows;
	}

	public boolean isFirst() {
		return first;
	}

	public void setFirst(boolean first) {
		this.first = first;
	}

	public boolean isLast() {
		return last;
	}

	public void setLast(boolean last) {
		this.last = last;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
}
