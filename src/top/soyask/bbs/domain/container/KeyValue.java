package top.soyask.bbs.domain.container;
/**
 * 用来存储两个关联对象
 *
 * @param <K>
 * @param <V>
 * <p>	
 *	Create on 2017年4月30日
 * @author mxf
 *
 */
public class KeyValue<K,V> {
	private K k;
	private V v;
	
	public KeyValue() {
	}
	
	public KeyValue(K k,V v){
		this.k = k;
		this.v = v;
	}
	
	public K getKey() {
		return k;
	}
	
	public V getValue() {
		return v;
	}
	
	public void setKey(K k) {
		this.k = k;
	}
	
	public void setValue(V v) {
		this.v = v;
	}
}
