package top.soyask.bbs.filter;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.function.BiConsumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import top.soyask.bbs.controller.base.BaseController;
import top.soyask.bbs.utils.ControllerCreator;
import top.soyask.bbs.utils.URLResolver;

/**
 * Servlet Filter implementation class CoreFilter
 */
@WebFilter("/*")
public class CoreFilter implements Filter {
	private Logger logger = Logger.getLogger("CoreFilter");
	private Properties controllers;
	private Properties jsps;

	/**
	 * Default constructor.
	 */
	public CoreFilter() { }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String url = httpRequest.getRequestURL().toString();
		String action = URLResolver.getMapping(url);
		String className = controllers.getProperty(action);
		if (className != null) {
			BaseController controller = null;
			try {
				ControllerCreator creator = ControllerCreator.getCreator();
				controller = creator.getController(httpRequest, className);
			} catch (Exception e) {
				e.printStackTrace();
			}
			String result = controller.execute(httpRequest);
			request.getRequestDispatcher(result).forward(request, response);
		} else {
			String jsp = jsps.getProperty(action);
			if(jsp != null){
				request.getRequestDispatcher(jsp).forward(request, response);
			}else {
				chain.doFilter(request, response);
			}
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		try {
			InputStream in = getClass().getClassLoader().getResourceAsStream("mapping.properties");
			controllers = new Properties();
			controllers.load(in);
			controllers.forEach(new BiConsumer<Object, Object>() {

				@Override
				public void accept(Object t, Object u) {
					logger.info("����ӳ�䣺"+t.toString()+"-------------"+u.toString());
				}
			});
			in.close();
			
			in = getClass().getClassLoader().getResourceAsStream("mapping-jsp.properties");
			jsps = new Properties();
			jsps.load(in);
			jsps.forEach(new BiConsumer<Object, Object>() {

				@Override
				public void accept(Object t, Object u) {
					logger.info("����ӳ�䣺"+t.toString()+"-------------"+u.toString());
				}
			});
			in.close();
		} catch (IOException e) {
//			e.printStackTrace();
			logger.log(Level.WARNING, e.getMessage());
		}
	}

}
