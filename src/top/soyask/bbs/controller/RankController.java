package top.soyask.bbs.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import top.soyask.bbs.controller.base.BaseController;
import top.soyask.bbs.domain.Page;
import top.soyask.bbs.domain.User;
import top.soyask.bbs.service.IUserInfoService;

public class RankController extends BaseController {

	private IUserInfoService userInfoService;
	private int page;

	@Override
	protected String process(HttpServletRequest request) {

		List<User> users = userInfoService.nextUserByExp(page, Page.ITEM_COUNT);
		Page p = userInfoService.userPage(page);
		request.setAttribute("users", users);
		request.setAttribute("page", p);
		return "WEB-INF/rank-list.jsp";
	}

}
