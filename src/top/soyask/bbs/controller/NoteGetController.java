package top.soyask.bbs.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import top.soyask.bbs.controller.base.BaseController;
import top.soyask.bbs.domain.Note;
import top.soyask.bbs.domain.Page;
import top.soyask.bbs.service.INoteService;

public class NoteGetController extends BaseController {

	private int noteType;
	private INoteService noteService;
	private int page;

	@Override
	protected String process(HttpServletRequest request) {
		List<Note> notes = noteService.nextNotes(noteType, page, Page.ITEM_COUNT);
		Page notePage = noteService.notePage(noteType,page);
		request.setAttribute("notes", notes);
		request.setAttribute("page", notePage);
		request.setAttribute("type", noteType);
		return "WEB-INF/note-list.jsp";
	}

	public INoteService getNoteService() {
		return noteService;
	}

	public void setNoteService(INoteService noteService) {
		this.noteService = noteService;
	}

	public int getNoteType() {
		return noteType;
	}

	public void setNoteType(int noteType) {
		this.noteType = noteType;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

}
