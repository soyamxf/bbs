package top.soyask.bbs.controller;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import top.soyask.bbs.controller.base.BaseController;

public class ExitController extends BaseController {

	@Override
	protected String process(HttpServletRequest request) {
		HttpSession session = request.getSession();
		Enumeration<String> attributeNames = session.getAttributeNames();
		while (attributeNames.hasMoreElements()) {
			String name = (String) attributeNames.nextElement();
			session.removeAttribute(name);
		}
		return "WEB-INF/success.jsp";
	}

}
