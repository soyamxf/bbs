package top.soyask.bbs.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import top.soyask.bbs.controller.base.BaseController;
import top.soyask.bbs.domain.User;
import top.soyask.bbs.service.IHandleService;
import top.soyask.bbs.service.IUserInfoService;

public class FollowController extends BaseController{
	
	private int uid;
	private IHandleService handleService;
	private IUserInfoService userInfoService;
	
	@Override
	protected String process(HttpServletRequest request) {
	
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute(User.CURRENT_USER);
		boolean result;
		if(user == null){
			return "WEB-INF/login.html";
		}
		
		List<Long> fansId = (List<Long>) session.getAttribute(User.FOLLOW);
		if( fansId.contains(Long.valueOf(uid))){
			result = handleService.unFollow(user.getUid(), uid);
		}else {
			result = handleService.follow(user.getUid(), uid);
		}
		
		if(result){
			fansId = userInfoService.followId(user.getUid());
			session.setAttribute(User.FOLLOW, fansId);	
			return "WEB-INF/success.jsp";
		}
		else {
			return "WEB-INF/error.jsp";
		}
	}

}
