package top.soyask.bbs.controller.base;

import javax.servlet.http.HttpServletRequest;

public abstract class BaseController {
		
	public BaseController() {

	}
	
	public String execute(HttpServletRequest request) {
		return process(request);
	}

	protected abstract String process(HttpServletRequest request);
	
}
