package top.soyask.bbs.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import top.soyask.bbs.controller.base.BaseController;
import top.soyask.bbs.domain.Note;
import top.soyask.bbs.domain.Page;
import top.soyask.bbs.domain.User;
import top.soyask.bbs.service.IUserInfoService;

public class NewsGetter extends BaseController {
	
	private IUserInfoService userInfoService;
	
	private int page;
	private long uid;
	
	@Override
	protected String process(HttpServletRequest request) {
		HttpSession session = request.getSession();
		if(uid == -1){
			User user = (User) session.getAttribute(User.CURRENT_USER);
			if(user != null){
				uid = user.getUid();
			}else {
				return "WEB-INF/none.jsp";
			}
		}
	
		List<Note> notes = userInfoService.nextNewNotes(uid, page, Page.ITEM_COUNT);
		Page newNotesPage = userInfoService.newNotesPage(uid,page);
		request.setAttribute("notes", notes);
		request.setAttribute("page", newNotesPage);
		if(notes.isEmpty()){
			return "WEB-INF/none.jsp";
		}
		return "WEB-INF/news-list.jsp";
	}

}
