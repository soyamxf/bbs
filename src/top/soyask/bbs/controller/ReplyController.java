package top.soyask.bbs.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import top.soyask.bbs.controller.base.BaseController;
import top.soyask.bbs.domain.Gobal;
import top.soyask.bbs.domain.Note;
import top.soyask.bbs.domain.User;
import top.soyask.bbs.service.INoteService;

public class ReplyController extends BaseController{
	
	private long mainId;
	private long target;
	private String content;
	private INoteService noteService;
	
	@Override
	protected String process(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute(User.CURRENT_USER);
		if(user == null){
			return "WEB-INF/login.html";
		}
		Note note = new Note();
		note.setContent(content);
		note.setCreateDate(new Date());
		note.setMainId(mainId);
		note.setType(Gobal.NORMAL_NOTE);
		note.setUser(user);
		if(noteService.reply(mainId, note)){
			return "WEB-INF/success.jsp";
		}else {
			return "WEB-INF/error.jsp";
		}
		
	}

}
