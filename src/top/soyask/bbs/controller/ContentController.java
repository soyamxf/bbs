package top.soyask.bbs.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import top.soyask.bbs.controller.base.BaseController;
import top.soyask.bbs.domain.Note;
import top.soyask.bbs.domain.Page;
import top.soyask.bbs.domain.User;
import top.soyask.bbs.domain.container.KeyValue;
import top.soyask.bbs.service.INoteService;

/**
 * 获取帖子点开后的列表
 *
 * <p>
 * Create on 2017年4月30日
 * 
 * @author mxf
 *
 */
public class ContentController extends BaseController {
	
	private long mainId;
	private INoteService noteService;
	private int page;

	@Override
	protected String process(HttpServletRequest request) {

		List<KeyValue<Note, User>> nextSubNotes = noteService.nextSubNotes(mainId, page, Page.ITEM_COUNT);
		Page p = noteService.subNotePage(mainId,page, Page.ITEM_COUNT);
		request.setAttribute("subNote", nextSubNotes);
		request.setAttribute("page", p);
		return "WEB-INF/content.jsp";
	}

	public INoteService getNoteService() {
		return noteService;
	}

	public void setNoteService(INoteService noteService) {
		this.noteService = noteService;
	}

	public long getMainId() {
		return mainId;
	}

	public void setMainId(long mainId) {
		this.mainId = mainId;
	}

}
