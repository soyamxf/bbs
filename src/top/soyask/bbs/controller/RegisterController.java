package top.soyask.bbs.controller;

import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import top.soyask.bbs.controller.base.BaseController;
import top.soyask.bbs.domain.User;
import top.soyask.bbs.service.IHandleService;

public class RegisterController extends BaseController {

	private String base;
	private String username;
	private String password;
	private IHandleService handleService;

	@Override
	protected String process(HttpServletRequest request) {

		String realPath = request.getServletContext().getRealPath("/");
		String filename = handleService.uploadPic(base,realPath);
		User user = new User();
		user.setPassword(password);
		user.setPicPath(filename);
		user.setUname(username);
		user.setInfo("这个人很懒，并没有留下什么...");
	
		Logger.getLogger("RegisterController").info(filename);
		if (handleService.regiser(user)) {
			return "WEB-INF/success.jsp";
		}
		return "WEB-INF/error.jsp";
	}

}
