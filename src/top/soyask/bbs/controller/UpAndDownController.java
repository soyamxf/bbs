package top.soyask.bbs.controller;

import javax.servlet.http.HttpServletRequest;

import top.soyask.bbs.controller.base.BaseController;
import top.soyask.bbs.service.INoteService;

public class UpAndDownController extends BaseController{
	private long mainId;
	private int type;
	private INoteService noteService;
	@Override
	protected String process(HttpServletRequest request) {
		if(type == 1){
			noteService.up(mainId);
		}else {
			noteService.down(mainId);
		}
		return "WEB-INF/success.jsp";
	}

}
