package top.soyask.bbs.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import top.soyask.bbs.controller.base.BaseController;
import top.soyask.bbs.domain.Page;
import top.soyask.bbs.domain.User;
import top.soyask.bbs.service.IUserInfoService;

public class FansGetter extends BaseController {
	private int page;
	private IUserInfoService userInfoService;
	private long uid;

	@Override
	protected String process(HttpServletRequest request) {
		HttpSession session = request.getSession();

		if (uid == -1) {
			User user = (User) session.getAttribute(User.CURRENT_USER);
			if (user == null) {
				return "WEB-INF/none.jsp";
			}
			uid = user.getUid();
		}

		Page fansPage = userInfoService.fansPage(uid,page);
		List<User> fans = userInfoService.nextFans(uid, page, Page.ITEM_COUNT);
		if (fans.isEmpty()) {
			return "WEB-INF/none.jsp";
		}
		request.setAttribute("fans", true);
		request.setAttribute("users", fans);
		request.setAttribute("page", fansPage);
		return "WEB-INF/fans-or-follow.jsp";
	}

}
