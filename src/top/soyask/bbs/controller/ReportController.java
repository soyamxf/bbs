package top.soyask.bbs.controller;

import java.util.Date;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import top.soyask.bbs.controller.base.BaseController;
import top.soyask.bbs.domain.Gobal;
import top.soyask.bbs.domain.Note;
import top.soyask.bbs.domain.User;
import top.soyask.bbs.service.IHandleService;
import top.soyask.bbs.service.INoteService;
import top.soyask.bbs.service.IUserInfoService;

public class ReportController extends BaseController {

	private String title;
	private String content;
	private INoteService noteService;
	private IHandleService userInfoService;

	@Override
	protected String process(HttpServletRequest request) {
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute(User.CURRENT_USER);
		if (user == null) {
			return "WEB-INF/error.jsp";
		} else {
			int exp = user.getExp();
			user.setExp(exp+100);
			userInfoService.updateInfo(user);
			session.setAttribute(User.CURRENT_USER, user);
			Note note = new Note();
			note.setTitle(title);
			note.setContent(content);
			note.setMainId(newMainId());
			note.setType(Gobal.SUBJECT);
			note.setCreateDate(new Date());
			note.setUname(user.getUname());
			note.setUid(user.getUid());
			note.setuPicPath(user.getPicPath()); // TODO
			note.setFloor(1);
			noteService.createNote(note);
		}
		return "WEB-INF/success.jsp";
	}

	public long newMainId() {
		long id = new Random(System.currentTimeMillis()).nextLong();
		return Math.abs(id >> 20);
	}

}
