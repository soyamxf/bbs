package top.soyask.bbs.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import top.soyask.bbs.controller.base.BaseController;
import top.soyask.bbs.domain.Page;
import top.soyask.bbs.domain.User;
import top.soyask.bbs.service.IUserInfoService;

public class FollowGetter extends BaseController {

	private int page;
	private long uid;
	private IUserInfoService userInfoService;

	@Override
	protected String process(HttpServletRequest request) {

		HttpSession session = request.getSession();

		if (uid == -1) {
			User user = (User) session.getAttribute(User.CURRENT_USER);
			if (user == null) {
				return "WEB-INF/none.jsp";
			}
			uid = user.getUid();
		}

		Page fPage = userInfoService.followPage(uid, page);
		List<User> follow = userInfoService.nextFollow(uid, page, Page.ITEM_COUNT);
		if (follow.isEmpty()) {
			return "WEB-INF/none.jsp";
		}
		request.setAttribute("users", follow);
		request.setAttribute("page", fPage);
		return "WEB-INF/fans-or-follow.jsp";
	}

}
