package top.soyask.bbs.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import top.soyask.bbs.controller.base.BaseController;
import top.soyask.bbs.domain.User;
import top.soyask.bbs.service.IHandleService;
import top.soyask.bbs.service.IUserInfoService;

public class LoginController extends BaseController {

	private IHandleService handleService;
	private IUserInfoService userInfoService;
	
	private long uid;
	private String password;

	@Override
	protected String process(HttpServletRequest request) {
		boolean login = handleService.login(uid, password);
		if (login) {
			User user = userInfoService.userInfo(uid);
			List<Long> fansId = userInfoService.followId(uid);
			
			HttpSession session = request.getSession();
			session.setAttribute(User.CURRENT_USER, user);
			session.setAttribute(User.FOLLOW, fansId);
			return "WEB-INF/success.jsp";
		}
		return "WEB-INF/error.jsp";
	}

}
