package top.soyask.bbs.controller;

import javax.servlet.http.HttpServletRequest;

import top.soyask.bbs.controller.base.BaseController;
import top.soyask.bbs.service.INoteService;


public class WatchController extends BaseController {
	
	private long mainId;
	private INoteService noteService;
	
	@Override
	protected String process(HttpServletRequest request) {
		try{
			noteService.watch(mainId);	
		}catch(Exception e){
			e.printStackTrace();
		}
		return "WEB-INF/note.jsp";
	}

}
