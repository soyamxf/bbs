package top.soyask.bbs.utils;

import java.util.ArrayList;
import java.util.List;

import top.soyask.bbs.domain.Page;

public class PageUtil {
	public static List<Integer> create(int current, int pageCount) {
		List<Integer> pages = new ArrayList<>();
		
		if (pageCount < 7) {
			for (int i = 1; i <= pageCount; i++) {
				pages.add(i);
			}
		} else {
			if (current < 4) {
				for (int i = 1; i <= 7; i++) {
					pages.add(i);
				}
			} else if (current > (pageCount - 3)) {
				for (int i = pageCount - 6; i <= pageCount; i++) {
					pages.add(i);
				}
			} else {
				for (int i = current - 3; i <= current + 3; i++) {
					pages.add(i);
				}
			}
		}
		return pages;
	}

	public static Page getPage(int current, int count) {
		count = (count - 1) / Page.ITEM_COUNT + 1;
		Page page = new Page();
		page.setCurrentPage(current);
		page.setCount(count);
		page.setRows(PageUtil.create(current, count));
		page.setFirst(current == 1);
		page.setLast(current == count);
		return page;
	}
}
