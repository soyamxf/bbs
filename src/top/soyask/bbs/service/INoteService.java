package top.soyask.bbs.service;

import java.util.List;

import top.soyask.bbs.domain.Note;
import top.soyask.bbs.domain.Page;
import top.soyask.bbs.domain.User;
import top.soyask.bbs.domain.container.KeyValue;

/**
 * 该类封装对帖子的各类操作
 *
 * <p>
 * Create on 2017年4月24日
 * 
 * @author mxf
 *
 */
public interface INoteService {

	List<Note> nextNewNotes(int page, int itemCount);

	List<Note> nextHotNotes(int page, int itemCount);

	List<Note> nextBestNotes(int page, int itemCount);

	List<Note> nextNotes(int noteType, int page, int itemCount);

	Page notePage(int type, int page);

	List<KeyValue<Note, User>> nextSubNotes(long mainId, int page, int itemCount);

	Page subNotePage(long mainId, int page, int itemCount);

	boolean up(long mainId);

	boolean down(long mainId);

	long createNote(Note note);

	boolean reply(long mainId, Note note);

	boolean reply(long mainId, long nid, Note note);

	boolean tip(long id, String content);

	void watch(long mainId);
}
