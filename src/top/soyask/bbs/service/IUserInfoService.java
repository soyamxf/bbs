package top.soyask.bbs.service;

import java.util.List;

import top.soyask.bbs.domain.Note;
import top.soyask.bbs.domain.Page;
import top.soyask.bbs.domain.User;

/**
 * 该类封装了用户的各类信息获取操作
 *
 * <p>
 * Create on 2017年4月24日
 * 
 * @author mxf
 *
 */
public interface IUserInfoService {
	User userInfo(long uid);

	List<Note> nextNewNotes(long uid, int page, int itemCount);

	Page newNotesPage(long uid, int current);

	List<User> nextUser(int page, int itemCount);

	List<User> nextUserByExp(int page, int itemCount);

	Page userPage(int current);

	List<User> nextFans(long uid, int page, int itemCount);
	
	List<Long> followId(long uid);

	Page fansPage(long uid, int current);

	List<User> nextFollow(long uid, int page, int itemCount);

	Page followPage(long uid, int current);
}
