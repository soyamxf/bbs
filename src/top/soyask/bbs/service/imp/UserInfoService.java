package top.soyask.bbs.service.imp;

import java.util.List;

import top.soyask.bbs.dao.IFansDao;
import top.soyask.bbs.dao.INoteDao;
import top.soyask.bbs.dao.IUserDao;
import top.soyask.bbs.dao.impl.FansDao;
import top.soyask.bbs.dao.impl.NoteDao;
import top.soyask.bbs.dao.impl.UserDao;
import top.soyask.bbs.domain.Gobal;
import top.soyask.bbs.domain.Note;
import top.soyask.bbs.domain.Page;
import top.soyask.bbs.domain.User;
import top.soyask.bbs.service.IUserInfoService;
import top.soyask.bbs.utils.PageUtil;

public class UserInfoService implements IUserInfoService {

	private IUserDao userDao = new UserDao();
	private INoteDao noteDao = new NoteDao();
	private IFansDao fansDao = new FansDao();

	@Override
	public User userInfo(long uid) {
		return userDao.get(uid);
	}

	@Override
	public List<Note> nextNewNotes(long uid, int page, int itemCount) {
		return noteDao.nextNewNotes(uid, page, itemCount, Gobal.NEW_NOTE);
	}

	@Override
	public Page newNotesPage(long uid, int current) {
		int count = (int) noteDao.count(uid);
		Page page = PageUtil.getPage(current, count);
		return page;
	}

	@Override
	public List<User> nextUser(int page, int itemCount) {
		return null;
	}

	@Override
	public List<User> nextUserByExp(int page, int itemCount) {
		List<User> users = userDao.nextUserByExp(page, itemCount);
		return users;
	}

	@Override
	public Page userPage(int current) {
		int count = (int) userDao.count();
		Page page = PageUtil.getPage(current, count);
		return page;
	}

	@Override
	public List<User> nextFans(long uid, int page, int itemCount) {

		return fansDao.nextFans(uid, page, itemCount);
	}

	@Override
	public Page fansPage(long uid, int current) {
		int fansCount = (int) fansDao.fansCount(uid);
		Page page = PageUtil.getPage(current, fansCount);
		return page;
	}

	@Override
	public List<User> nextFollow(long uid, int page, int itemCount) {
		return fansDao.nextFollow(uid, page, itemCount);
	}

	@Override
	public Page followPage(long uid, int current) {
		int count = (int) fansDao.followCount(uid);
		Page page = PageUtil.getPage(current, count);
		return page;
	}

	@Override
	public List<Long> followId(long uid) {
		List<Long> fans = fansDao.followId(uid);
		return fans;
	}

}
