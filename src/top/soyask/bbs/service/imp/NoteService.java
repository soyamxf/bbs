package top.soyask.bbs.service.imp;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import top.soyask.bbs.dao.INoteDao;
import top.soyask.bbs.dao.impl.NoteDao;
import top.soyask.bbs.domain.Gobal;
import top.soyask.bbs.domain.Note;
import top.soyask.bbs.domain.Page;
import top.soyask.bbs.domain.User;
import top.soyask.bbs.domain.container.KeyValue;
import top.soyask.bbs.service.INoteService;
import top.soyask.bbs.utils.PageUtil;

public class NoteService implements INoteService {

	private INoteDao noteDao = new NoteDao();

	@Override
	public List<Note> nextNewNotes(int page, int itemCount) {
		return noteDao.nextNewNotes(page, itemCount);
	}

	@Override
	public List<Note> nextHotNotes(int page, int itemCount) {
		return noteDao.nextHotNotes(page, itemCount);
	}

	@Override
	public List<Note> nextBestNotes(int page, int itemCount) {
		return noteDao.nextBestNotes(page, itemCount);
	}

	@Override
	public List<Note> nextNotes(int noteType, int page, int itemCount) {
		List<Note> notes = null;
		if (Gobal.NEW_NOTE == noteType) {
			notes = nextNewNotes(page, itemCount);
		} else if (Gobal.HOT_NOTE == noteType) {
			notes = nextHotNotes(page, itemCount);
		} else {
			notes = nextBestNotes(page, itemCount);
		}
		return notes;
	}

	@Override
	public Page notePage(int type, int current) {
		int count = (int) noteDao.count(type);
		Page page = PageUtil.getPage(current, count);
		return page;
	}

	@Override
	public List<KeyValue<Note, User>> nextSubNotes(long mainId, int page, int itemCount) {
		List<Map<String, Object>> subNotes = noteDao.nextSubNotes(mainId, page, itemCount);
		List<KeyValue<Note, User>> keyValues = new ArrayList<>();
		for (Map<String, Object> map : subNotes) {
			User user = new User();
			user.setUid((long) map.get("uid"));
			user.setUname((String) map.get("uname"));
			user.setFans((int) map.get("fans"));
			user.setFollow((int) map.get("follow"));
			user.setInfo((String) map.get("info"));
			user.setExp((int) map.get("exp"));

			Note note = new Note();
			note.setContent((String) map.get("content"));
			note.setCreateDate((Date) map.get("createDate"));
			note.setFloor((int) map.get("floor"));
			note.setMainId((long) map.get("mainId"));
			note.setNid((long) map.get("nid"));
			note.setUser(user);
			note.setTitle((String) map.get("title"));

			KeyValue<Note, User> kv = new KeyValue<>(note, user);
			keyValues.add(kv);
		}
		return keyValues;
	}

	@Override
	public Page subNotePage(long mainId, int current, int itemCount) {
		int count = (int) noteDao.subCount(mainId);
		Page page = PageUtil.getPage(current, count);
		return page;
	}

	@Override
	public boolean up(long mainId) {
		noteDao.up(mainId);
		return true;
	}

	@Override
	public boolean down(long mainId) {
		noteDao.down(mainId);
		return true;
	}

	@Override
	public long createNote(Note note) {
		return noteDao.create(note);
	}

	@Override
	public boolean reply(long mainId, Note note) {
		Note main = noteDao.getNote(mainId);
		if (main != null) {
			int reply = main.getReply() + 1;
			main.setReply(reply);
			note.setFloor(reply + 1);
			noteDao.update(main);
			noteDao.create(note);
			return true;
		}
		return false;
	}

	@Override
	public boolean reply(long mainId, long nid, Note note) {
		return true;
	}

	@Override
	public boolean tip(long id, String content) {
		return true;
	}

	@Override
	public void watch(long mainId) {
		noteDao.watch(mainId);
	}

}
