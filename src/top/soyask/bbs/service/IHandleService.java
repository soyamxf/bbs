package top.soyask.bbs.service;

import java.io.File;

import top.soyask.bbs.domain.User;

/**
 * 该类封装用户的各类操作 
 *
 * <p>	
 *	Create on 2017年4月24日
 * @author mxf
 *
 */
public interface IHandleService {
	/**
	 * 上传头像图片
	 * @param file 头像的图片
	 * @return 该头像存储的路径
	 */
	String uploadPic(String base64,String realPath);
	
	/**
	 * 注册用户
	 * @param user 注册的用户信息
	 * @return 注册成功则回复true
	 */
	boolean regiser(User user);
	boolean login(long uid,String password);
	boolean updateInfo(User user);

	/**
	 * 关注用户
	 * @param uid 当前用户的id
	 * @param targetId 关注对象的id
	 * @return 关注成功则返回true
	 */
	boolean follow(long uid,long targetId);

	boolean unFollow(long uid, int uid2);
}
